[![pipeline status](https://gitlab.com/grunlab/gerbera/badges/main/pipeline.svg)](https://gitlab.com/grunlab/gerbera/-/commits/main)

# GrunLab Gerbera

Gerbera non-root container image and deployment on Kubernetes.

Docs:
- https://docs.grunlab.net/images/gerbera.md
- https://docs.grunlab.net/apps/gerbera.md

Base image: [grunlab/base-image/ubuntu:24.04][base-image]

Format: docker

Supported architecture(s):
- arm64

[base-image]: <https://gitlab.com/grunlab/base-image>