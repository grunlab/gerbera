#!/bin/bash

# config.xml
if [ ! -f /mnt/gerbera-config/config.xml ]; then cp /etc/gerbera/config.xml /mnt/gerbera-config/config.xml; fi
rm /etc/gerbera/config.xml
ln -s /mnt/gerbera-config/config.xml /etc/gerbera/config.xml

# start gerbera
gerbera -c /etc/gerbera/config.xml

